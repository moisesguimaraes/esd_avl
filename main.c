#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "avl.h"

void helper(void) {
    printf("uso: PROG [argumentos]\n"
        "\nonde os argumentos sao:"
        "\n\t n   : Insere o numero 'n' na arvore."
        "\n\t-n   : Remove o elemento 'n' da arvore."
        "\n\t pre : Imprime a arvore de forma preorder."
        "\n\t pos : Imprime a arvore de forma posorder."
        "\n\t in  : Imprime a arvore de forma inorder."
        "\n\t gra : Imprime a arvore de forma grafica."
        "\n");
}

int main(int argc, char** argv)
{
    int index, n;
    AVL *t = avl_create();
    
    if(argc < 2)
        helper();
    
    if (t == NULL)
        return memory_e;
    
    for (index = 1; index < argc; index++) {
        switch (argv[index][0]) {
            case 'p':
                if (!strcmp(argv[index], "pre"))
                    avl_preorder(t, print_avl_node);
                else if (!strcmp(argv[index], "pos"))
                    avl_posorder(t, print_avl_node);
            break;
        
            case 'i':
                if (!strcmp(argv[index], "in"))
                    avl_inorder(t, print_avl_node);
            break;
            
            case 'g':
                if (!strcmp(argv[index], "gra"))
                    print_avl(t);
            break;
        
            case '-':
                avl_remove(t, - atoi(argv[index]))
            break;
                        
            default:
                avl_insert(t, atoi(argv[index]));
            break;    
        }
    }
    
    avl_destroy(l);
    
    return success;
}
