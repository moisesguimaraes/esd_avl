#ifndef AVL_H
#define AVL_H

typedef struct n {
    int data;
    int height;
    int factor;
    struct n *left;
    struct n *right;
} AVLNode;

typedef struct {
    AVLNode *root;
} AVL;

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

/* Códigos de retorno */
enum {
    success   = 0, /* operação realizada com sucesso. */
    param_e   = 1, /* erro de parâmetro inválido.     */
    memory_e  = 2, /* erro de alocação de memória.    */
    unknown_e = 3, /* erro desconhecido.              */
};

typedef void (*avl_func)(AVLNode *node);

AVL* avl_create();
void avl_destroy(AVL *avl);

int avl_insert(AVL *avl, int data);
int avl_remove(AVL *avl, int data);

void avl_preorder(AVL *avl, avl_func *f);
void avl_posorder(AVL *avl, avl_func *f);
void avl_inorder (AVL *avl, avl_func *f);

#endif